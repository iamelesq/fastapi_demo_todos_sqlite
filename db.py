from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker

DB_URI = "sqlite:///./todoapp.db"

# By default sqlite doesn't allow multiple requests to run
# in tandem the below setting works around the fact that
# fastAPI does perform multiple requests in tandem by
# default. Disabling check_same_thread forces each request
# to be an individual this working with fastAPI.
engine = create_engine(DB_URI, connect_args={"check_same_thread": False})

# used to initialise the database sessions. Named SessionLocal
# in order to avoid collision with the Session type imported
# from FastAPI itself.
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# base class used to create database models that can relate to
# python classes.
Base = declarative_base()

# used as a dependency to control opening and closing
# the database.
class DBcontext:
    def __init__(self):
        self.db = SessionLocal()

    def __enter__(self):
        return self.db

    def __exit__(self, execType, execValue, traceback):
        self.db.close()
