from datetime import timedelta
from fastapi import FastAPI, Request, Depends, status, Form, Response, Path
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.encoders import jsonable_encoder
from fastapi_login import LoginManager
from dotenv import load_dotenv
import os

from passlib.context import CryptContext
from requests import session


# database related
import models, crud, schemas
from db import SessionLocal, engine, DBcontext
from sqlalchemy.orm import Session

# load env
load_dotenv()
SECRET_KEY = os.getenv("SECRET_KEY")
TOKEN_EXPIRY_MINUTES = 60

# sessionManager
manager = LoginManager(SECRET_KEY, token_url="/login", use_cookie=True)
manager.cookie_name = "auth"
pwd_ctx = CryptContext(schemes=["bcrypt"], deprecated="auto")

app = FastAPI()
templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")


def get_db():
    with DBcontext() as db:
        yield db


def get_hashed_password(password):
    return pwd_ctx.hash(password)


def verify_password(password, hashed_password):
    return pwd_ctx.verify(password, hashed_password)


class NotAuthenticatedException(Exception):
    pass


def not_authenticated_exception_handler(request, exception):
    return RedirectResponse("/login")


manager.not_authenticated_exception = NotAuthenticatedException
app.add_exception_handler(
    NotAuthenticatedException, not_authenticated_exception_handler
)


@manager.user_loader()
def get_user(username: str, db: Session = None):
    if db is None:
        with DBcontext() as db:
            return crud.get_user_by_username(db=db, username=username)
    return crud.get_user_by_username(db=db, username=username)


def authenticate_user(username: str, password: str, db: Session = Depends(get_db)):
    user = crud.get_user_by_username(username=username, db=db)
    if not user:
        return None
    if not verify_password(password=password, hashed_password=user.hashed_password):
        return None
    return user


@app.get("/", response_class=HTMLResponse)
async def root(request: Request):
    return templates.TemplateResponse(
        "index.html", {"request": request, "title": "Home"}
    )


@app.get("/tasks")
async def get_task(
    request: Request,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(manager),
):
    return templates.TemplateResponse(
        "tasks.html",
        {
            "request": request,
            "title": "tasks",
            "user": user,
            "tasks": crud.get_tasks_by_user_id(db=db, id=user.id),
        },
    )


@app.post("/tasks")
async def add_task(
    request: Request,
    text: str = Form(...),
    db: Session = Depends(get_db),
    user: schemas.User = Depends(manager),
):
    added = crud.add_task(db=db, task=schemas.TaskCreate(text=text), id=user.id)
    if not added:
        return templates.TemplateResponse(
            "tasks.html",
            {
                "request": request,
                "title": "Tasks",
                "user": user,
                "tasks": crud.get_tasks_by_user_id(db=db, id=user.id),
                "invalid": True,
            },
            status_code=status.HTTP_400_BAD_REQUEST,
        )
    else:
        return RedirectResponse("/tasks", status_code=status.HTTP_302_FOUND)


@app.get("/tasks/delete/{id}", response_class=RedirectResponse)
def delete_task(
    id: str = Path(...),
    db: Session = Depends(get_db),
    user: schemas.User = Depends(manager),
):
    crud.delete_task(db=db, id=id)
    return RedirectResponse("/tasks")


@app.get("/login")
async def get_login(request: Request):
    return templates.TemplateResponse(
        "login.html", {"request": request, "title": "login"}
    )


@app.post("/login")
async def login(
    request: Request,
    form_data: OAuth2PasswordRequestForm = Depends(OAuth2PasswordRequestForm),
    db: Session = Depends(get_db),
):
    user = authenticate_user(
        username=form_data.username, password=form_data.password, db=db
    )
    if not user:
        return templates.TemplateResponse(
            "login.html",
            {"request": request, "title": "Login", "invalid": True},
            status_code=status.HTTP_401_UNAUTHORIZED,
        )
    access_token_expiry = timedelta(minutes=TOKEN_EXPIRY_MINUTES)
    access_token = manager.create_access_token(
        data={"sub": user.username}, expires=access_token_expiry
    )
    response = RedirectResponse("/tasks", status_code=status.HTTP_302_FOUND)
    manager.set_cookie(response, access_token)
    return response


@app.get("/register")
async def get_register(request: Request):
    return templates.TemplateResponse(
        "register.html", {"request": request, "title": "Register"}
    )


@app.post("/register")
async def register(
    request: Request,
    username: str = Form(...),
    name: str = Form(...),
    email: str = Form(...),
    password: str = Form(...),
    db: session = Depends(get_db),
):
    hashed_password = get_hashed_password(password=password)
    invalid = False
    if crud.get_user_by_username(db=db, username=username):
        invalid = True
    if crud.get_user_by_email(db=db, email=email):
        invalid = True
    if not invalid:
        crud.create_user(
            db=db,
            user=schemas.UserCreate(
                username=username,
                name=name,
                email=email,
                hashed_password=hashed_password,
            ),
        )
        respose = RedirectResponse("/login", status_code=status.HTTP_302_FOUND)
        return respose
    else:
        return templates.TemplateResponse(
            "register.html",
            {"request": request, "title": "Register", "invalid": True},
            status_code=status.HTTP_400_BAD_REQUEST,
        )


@app.get("/logout")
async def logout(response: Response):
    response = RedirectResponse("/")
    manager.set_cookie(response, None)
    return response
