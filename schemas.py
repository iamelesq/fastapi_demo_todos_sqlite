import email
from unicodedata import name
from pydantic import BaseModel
from typing import Optional, List

# schemas are used to work with data itself, we use models to
# work dorectly with the database.

# Task schemas
class TaskBase(BaseModel):
    text: str


class Task(TaskBase):
    id: str
    user_id: str

    # configure orm to work
    class config:
        orm_mode = True


class TaskCreate(TaskBase):
    pass


# User schemas
class UserBase(BaseModel):
    username: str
    email: str
    name: str
    hashed_password: str


class User(UserBase):
    id: str
    tasks: List[Task] = []

    class config:
        orm_mode = True


class UserCreate(UserBase):
    pass
